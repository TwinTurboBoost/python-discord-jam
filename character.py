from tabulate import tabulate


class Character:

    def __init__(self, inventory):
        """
        Creates a new instance of
        a character, and sets up
        their inventory
        """

        assert isinstance(inventory, list)
        self.inventory = inventory

    def convert_inventory(self, inventory):
        """
        :param inventory: list of strings with a : in each string separating the item
        from the type
        :return: dict finalDict
        takes in the inventory, and for each string splits it up into the item and its type
        before placing it in a dictionary
        """
        finalDict = {}
        for invstring in inventory:
            item, itemtype = invstring.split(":")
            if itemtype in finalDict.keys():
                finalDict[itemtype].append(item)
            else:
                finalDict[itemtype] = []
                finalDict[itemtype].append(item)
        return finalDict



    def display_inventory(self):
        """
        Displays the inventory with tabulate.
        """

        # First, convert the inventory list to a dictionary
        inventory = self.convert_inventory(self.inventory)

        # Now we can display the inventory!
        print(tabulate(inventory, headers="keys"))
        print("\n")


inventory = [
    "lemon-flavored lemons:food",
    "apple-flavored lemons:food",
    "bbqdude's chicken:food",
    "bisk's magic stew:food",
    "metaclasses:magic",
    "recursion:magic",
    "abstract base classes:magic",
    "pantsuit:armor",
    "pony ears:armor",
    "joseph's ushanka:armor",
    "4.1513:bitcoins",
]

lucy = Character(inventory)
lucy.display_inventory()